#pragma once
#include <string>
#include <array>
#include <map>

#include <ArduinoJson.h>
#include <vector>


//RTC_DATA_ATTR uint16_t internal_idx;

struct Measurement
{
    static std::uint16_t& get_global_idx()
    {
        RTC_DATA_ATTR static std::uint16_t idx = 0;
        return idx;
    }
    //static std::uint16_t internal_idx = 0; // Zero is not legal idx
    uint32_t unixtime=0;
    std::map<std::string, float> variables;
    std::uint16_t idx;
    /*std::string to_url(const std::string &prefix = "")
    {
        std::string ret = prefix;
        for (auto iter=variables.begin(); iter!=variables.end(); iter++)
        {
            ret = ret +  iter->first + "=" + std::to_string(iter->second) + "&";
        }

        ret = ret + "time="+std::to_string(unixtime);
        return ret;
    } */   

    std::string to_json() const
    {
        DynamicJsonDocument doc(1024);

        for (auto iter = variables.begin(); iter != variables.end(); iter++)
        {
            std::string key = iter->first;
            float value = iter->second;
            doc[key] = value;
        }
        doc["unixtime"] = unixtime;
        doc["idx"] = this->idx;
        std::string jsonstr;
        serializeJson(doc, jsonstr);
        return jsonstr;
    }
    
    float& operator[](const std::string &var)
    {
        /*auto iter = variables.find(var);
        if (iter != variables.end())
            return iter->second;
        return -3e30;*/
        return variables[var];
    }

    const float& operator[](const std::string &var) const
    {
        Measurement *m = const_cast<Measurement*>(this);
        return (*m)[var];
        /*auto iter = variables.find(var);
        if (iter != variables.end())
            return iter->second;*/
    }

    void clear()
    {
        variables.clear();
    }

    Measurement()
    {
        auto &internal_idx = get_global_idx();
        internal_idx = internal_idx + 1;
        //Serial.printf("Internal_IDX: %i\n", internal_idx);
        this->idx = internal_idx;
    }

    //std::array<std::pair<std::string, float>, SIZE > variables;
    /*std::array<std::string, SIZE> variables;
    std::array<float, SIZE> values;
    std::string variables[measurement_size];
    float values[measurement_size];*/

    /*int add_variable(const std::string &name)
    {
        for (int k=0; k<measurement_size; k++)
        {
            if (variables[k].empty())
            {
                variables[k] = 
            }
        }
    }*/

};

//uint16_t Measurement::internal_idx = 0;


const int max_measurements = 1000;
std::vector<Measurement> measurements;

SemaphoreHandle_t measurement_mutex = xSemaphoreCreateMutex();


void add_measurement(const Measurement &m)
{
    if (!xSemaphoreTake( measurement_mutex, portMAX_DELAY))
        return;

    measurements.push_back(m);
    if (measurements.size() > max_measurements)
        measurements.erase(measurements.begin());
    xSemaphoreGive(measurement_mutex);
}
void del_first_measurement()
{

    if (!xSemaphoreTake( measurement_mutex, portMAX_DELAY))
        return;
    if (!measurements.empty())
        measurements.erase(measurements.begin());
    xSemaphoreGive(measurement_mutex);
}

void del_idx_measurement(uint16_t idx)
{
    if (!xSemaphoreTake( measurement_mutex, portMAX_DELAY))
        return;
    measurements.erase(
    std::remove_if(measurements.begin(), measurements.end(),
        [idx](const Measurement & m) { return m.idx == idx; }),
    measurements.end());

    xSemaphoreGive(measurement_mutex);

}
