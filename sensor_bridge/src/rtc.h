#pragma once

#include <Wire.h>
#include <EepromAT24C32.h>
#include <RtcDS3231.h>
#include <sys/time.h>
#include <random>
#include "settings.h"

RtcDS3231<TwoWire> rtc(Wire);
EepromAt24c32<TwoWire> rtc_eeprom(Wire);

int32_t rtc_max_difference = 2678400; // 31 days
bool clock_set = false;

uint32_t get_clock();

#define countof(a) (sizeof(a) / sizeof(a[0]))

std::string unixtime2str(uint32_t unixtime, const char* format)
{



  char buffer[256];

  struct tm timeinfo;
  time_t seconds = unixtime;
  memcpy(&timeinfo, localtime(&seconds), sizeof (struct tm));

  /*time_t t = unixtime;
  struct tm timeinfo = localtime(&t);
  //struct tm timeinfo;
  localtime()*/
  /*if (!getLocalTime(&timeinfo)) {
    Serial.println("Failed to obtain time");
    return "";
  }*/
  size_t len = strftime(buffer, 256, format, &timeinfo);
  return std::string(buffer, len);
}

std::string clock2str(const char* format)
{
  uint32_t unixtime = get_clock();
  return unixtime2str(unixtime, format);
}


std::string datetime2str(const RtcDateTime& dt)
{
    char datestring[20];

    snprintf_P(datestring, 
            countof(datestring),
            PSTR("%02u/%02u/%04u %02u:%02u:%02u"),
            dt.Month(),
            dt.Day(),
            dt.Year(),
            dt.Hour(),
            dt.Minute(),
            dt.Second() );
    return std::string(datestring);
    //Serial.print(datestring);
}

void init_rtc()
{
    rtc.Begin();
    rtc_eeprom.Begin();

    // never assume the Rtc was last configured by you, so
    // just clear them to your needed state
    rtc.SetIsRunning(true);
    rtc.Enable32kHzPin(false);
    rtc.SetSquareWavePin(DS3231SquareWavePin_ModeNone); 
    if (rtc.GetIsRunning() && rtc.IsDateTimeValid())
      Serial.printf("RTC time is: %s\n", datetime2str(rtc.GetDateTime()).c_str());
    else
      Serial.println("RTC is not valid.");
}

void set_rtc_lastupdate(uint32_t ts)
{
    //uint8_t bytes[4];
    //uint32_t ts = rtc.GetDateTime().Epoch32Time();
    rtc_eeprom.SetMemory(0, (uint8_t*)&ts, 4);
}

uint32_t get_rtc_lastupdate()
{
    uint32_t ts=0;
    rtc_eeprom.GetMemory(0, (uint8_t*)&ts, 4);
    return ts;
}

void set_clock_from_rtc()
{
  struct timeval tv;
  tv.tv_sec = rtc.GetDateTime().Epoch32Time();
  tv.tv_usec = 0;
  settimeofday(&tv, nullptr);
}

void set_clock_from_ntp()
{
    std::string ntpServer = settings["ntp"]["server"].as<std::string>();
    const long  gmtOffset_sec = settings["ntp"]["gmt_offset"].as<long>();
    const int   daylightOffset_sec = settings["ntp"]["daylight_offset"].as<int>();
    
    configTime(gmtOffset_sec, daylightOffset_sec, ntpServer.c_str());
}

void set_rtc_from_clock()
{
    struct tm time;
    RtcDateTime dt;
    getLocalTime(&time);
    dt.InitWithEpoch32Time(mktime(&time));
    rtc.SetDateTime(dt);
}

// Function that gets current epoch time
uint32_t get_clock() {
  time_t now;
  struct tm timeinfo;
  if (!getLocalTime(&timeinfo)) {
    Serial.println("Failed to obtain time");
    return(0);
  }
  time(&now);
  return now;
}

void set_clock(uint32_t unixtime)
{
  struct timeval tv;
  tv.tv_sec = unixtime;
  tv.tv_usec = 0;
  settimeofday(&tv, nullptr);
}

/*
Best source for internal clock is ntp, second best is hardware rtc. Ntp should be used sparingly
since it requires wifi traffic (power / latency) and remote servers might not like to frequent polling.
1. If wifi is available and value in rtc is not valid: updates clock from ntp then update rtc.
2. if no wifi then update clock from rtc.
*/
void set_clock_best_effort()
{

  if (WiFi.isConnected())
  {
    bool update_from_internet = false;

    // If rtc is not valid..
    if (!rtc.IsDateTimeValid())
    {
      update_from_internet = true;
    }
    else if (esp_random() % 100 == 0)
    {
      // Update rtc from time to time even if rtc is valid.
        update_from_internet = true;
    }
    else
    {
      uint32_t rtc_unixtime = rtc.GetDateTime().Epoch32Time();
      uint32_t rtc_lastupdate = get_rtc_lastupdate();
      if (rtc_lastupdate > rtc_unixtime || rtc_unixtime - rtc_lastupdate > rtc_max_difference)
      {
        update_from_internet = true;
      }
      else
      {
        set_clock_from_rtc();
      }
    }
    if (update_from_internet)
    {
      set_clock_from_ntp();
      set_rtc_from_clock();
      set_rtc_lastupdate(get_clock());
      clock_set = true;
    }
  }
  else{
    set_clock_from_rtc();
    clock_set = true;
  }

    RtcDateTime dt;
    dt.InitWithEpoch32Time(get_clock());

    Serial.printf("System clock: %s\n", datetime2str(dt).c_str());
  
}
