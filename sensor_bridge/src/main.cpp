
//#define _GLIBCXX_USE_C99


#include <esp_now.h>
#include <WiFi.h>
#include <esp_wifi.h>
#include "driver/uart.h"

//#include "measurement.h"
#include <ArduinoJson.h>
#include "SPIFFS.h"
#include <string>
#include "rtc.h"


//#define TRANSMIT 1
//#include <Adafruit_Sensor.h>

//PubSubClient client(espClient);

//RTC_DS3231 rtc;



//uint8_t peer_mac[] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
//uint8_t *peer_mac;

uint8_t transmitter_mac[] = {0x34, 0x86, 0x5D, 0x3A, 0x34, 0x0C};
uint8_t receiver_mac[] = {0x34, 0x86, 0x5D, 0x3A, 0x0E, 0x44};

#if TRANSMIT
uint8_t *peer_mac = receiver_mac;
#else
uint8_t *peer_mac = transmitter_mac;
#endif



esp_now_peer_info_t peerInfo;

std::string mac_arr2str(const uint8_t *mac)
{
  char buff[255];
  int len = sprintf(buff, "%X:%X:%X:%X:%X:%X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
  return std::string(buff, len);
}

void mac_str2arr(const std::string macstr, uint8_t *mac)
{
  //"34:86:5D:3A:34:C"
  size_t pos = 0;
  size_t prev = 0;
  pos = macstr.find(':');
  std::string tmp = "";
  int i = 0;
  while (pos != macstr.npos)
  {
    tmp = macstr.substr(prev, pos-prev);
    //int i = std::stoul(tmp, nullptr, 16);
    mac[i] = std::stoul(tmp, nullptr, 16);
    //Serial.printf(">%i, %s, %i ", pos, tmp.c_str(), i);
    prev = pos+1;
    pos = macstr.find(':', pos+1);
    i++;
  }
  tmp = macstr.substr(prev);
  mac[i] = std::stoul(tmp, nullptr, 16);

  //Serial.printf(">%s ",tmp.c_str());
    
  //Serial.println();
}

void print_mac(const uint8_t *mac)
{
  for (int i=0; i<6; i++) Serial.printf("%X ", mac[i]);
}

// callback when data is sent
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  //Serial.print("\r\nLast Packet Send Status:\t");
  Serial.print("Last packed sent to ");
  print_mac(mac_addr);
  Serial.print(": ");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
}

// callback function that will be executed when data is received
void OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len) {
  /*assert(len == sizeof(uint32_t));
  uint32_t *cnt = (uint32_t*)incomingData;*/
  //if (incomingData[len-1] == '\0')
  std::string str((const char*)incomingData, len);
  Serial.printf("Received %i bytes from ", len);
  print_mac(mac);
  Serial.printf(": %s\n", str.c_str());
  /*if (len>=4)
  {
    uint32_t *i = (uint32_t*)&incomingData[0];
    Serial.printf("Number: %i\n", *i);
  }*/
  Serial2.write((const char*)incomingData, len);
  if (incomingData[len-1] != '\0')
    Serial2.write('\0');
  //Serial2.write('\0');
  //Serial2.printf("%s", str.c_str());
}




void uart_send(void * parameter)
{
  const int TXD_PIN = GPIO_NUM_17;
  const int RXD_PIN = GPIO_NUM_16;
  /*const int SERIAL_SIZE_RX = 1024;
  const uart_config_t uart_config = {
        .baud_rate = 9600,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE
    };
    uart_param_config(UART_NUM_1, &uart_config);
    uart_set_pin(UART_NUM_1, TXD_PIN, RXD_PIN, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
    // We won't use a buffer for sending data.
    uart_driver_install(UART_NUM_1, SERIAL_SIZE_RX, 0, 0, NULL, 0);*/
  Serial1.begin(9600, SERIAL_8N1, RXD_PIN, TXD_PIN);
  while (true)
  {
    //Serial1.readString()
    //if (!measurements.empty())
    {

    }
  }    
}

void load_peers()
{
  File file = SPIFFS.open("/espnow_peers.json","r");
  if(!file)
  {
      Serial.println("Failed to open file for reading");
      return;
  }
  String jsonstr = file.readString();
  file.close();

  StaticJsonDocument<1024*5> json;
  deserializeJson(json, jsonstr);

  //auto j_peers = json["peers"];

  uint8_t mac_addrs[6];
  for (int i=0; i<6; i++) mac_addrs[i] = 0;

  for (auto peer : json["peers"].as<JsonArray>()) {
    int idx = 0;
    if (peer["mac"].is<std::string>())
    {
      mac_str2arr(peer["mac"].as<std::string>(), mac_addrs);
    }
    else
    {
      for (auto macnum : peer["mac"].as<JsonArray>())
      {
        mac_addrs[idx] = macnum.as<uint8_t>();
        idx++;
      }
    }

    Serial.print("Mac address for peer: ");
    print_mac(mac_addrs);
    //Serial.println("");
    
    memcpy(peerInfo.peer_addr, mac_addrs, 6);
    peerInfo.channel = 0;  
    peerInfo.encrypt = false;
    
    // Add peer        
    if (esp_now_add_peer(&peerInfo) != ESP_OK){
      Serial.println("  [FAIL]");
    }
    else Serial.println("  [OK]");
  }
  /*for (JsonPair keyValue : json["peers"].as<JsonArray>()) {
    auto macs = keyValue.value()["mac"];
    mac_addrs[0] = macs[0].as<uint8_t>();
    mac_addrs[1] = macs[1].as<uint8_t>();
    mac_addrs[2] = macs[2].as<uint8_t>();
    mac_addrs[3] = macs[3].as<uint8_t>();
    mac_addrs[4] = macs[4].as<uint8_t>();
    mac_addrs[5] = macs[5].as<uint8_t>();
    
  }*/
  
  
}

void execute_commands(DynamicJsonDocument &command)
{
    if (command.containsKey("command"))
    {
      std::string command_str = command["command"].as<std::string>();
      Serial.printf("Received command: %s\n", command_str.c_str());
      if (command_str == "set_date")
      {
        uint32_t unixtime = command["unixtime"].as<uint32_t>();
        struct timeval tv;
        tv.tv_sec = unixtime;
        tv.tv_usec = 0;
        settimeofday(&tv, nullptr);
        RtcDateTime dt;
        dt.InitWithEpoch32Time(get_clock());

        Serial.printf("System clock: %s\n", datetime2str(dt).c_str());
      }
      if (command_str == "overwritefile")
      {
        std::string filename = command["filename"].as<std::string>();
        std::string content = command["content"].as<std::string>();
        Serial.printf("Filename: %s\nContent: %s\n", filename.c_str(), content.c_str());
        File file = SPIFFS.open(filename.c_str(),"w+");
        if(!file)
        {
            Serial.println("Failed to open file for writing");
            return;
        }
        Serial.println("Writing content");
        file.print(content.c_str());
        Serial.println("Closing file.");
        file.close();
        Serial.println("Delay");
        delay(1000);
        Serial.println("Restart");
        ESP.restart();
      }

    }
}

std::string poll_uart2(bool block = false)
{
  uint8_t buffer[1024];
  std::string txt = "";
  while (Serial2.available())
  {
      size_t len = Serial2.readBytesUntil('\0', buffer, 1024);
      if (len == 0)
        break;
      txt = txt + std::string((char*)buffer, len);
  }
  return txt;
}


void setup()
{
  Serial.begin(9600);
  while (!Serial);

  const int TXD_PIN = GPIO_NUM_17;
  const int RXD_PIN = GPIO_NUM_16;
  Serial2.begin(9600, SERIAL_8N1, RXD_PIN, TXD_PIN);
  while (!Serial2);


  WiFi.mode(WIFI_MODE_STA);

  #if TRANSMIT
    esp_wifi_set_mac(WIFI_IF_STA, &transmitter_mac[0]);
  #else
    esp_wifi_set_mac(WIFI_IF_STA, &receiver_mac[0]);
  #endif
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  Serial.println(WiFi.macAddress());
  //mac_str2arr("34:86:5D:3A:34:C");

  if(!SPIFFS.begin()){
    Serial.println("An Error has occurred while mounting SPIFFS");
    while (true);
  }

  // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Trasnmitted packet
  esp_now_register_send_cb(OnDataSent);
  

  // Once ESPNow is successfully Init, we will register for recv CB to
  // get recv packer info
  esp_now_register_recv_cb(OnDataRecv);

  
  load_peers();
  // Register peer
  /*memcpy(peerInfo.peer_addr, peer_mac, 6);
  peerInfo.channel = 0;  
  peerInfo.encrypt = false;
  
  // Add peer        
  if (esp_now_add_peer(&peerInfo) != ESP_OK){
    Serial.println("Failed to add peer");
    return;
  }*/

  #if TRANSMIT
  Serial.println("This is Transmiter");
  #else
  Serial.println("This is Receiver");
  #endif


  /*xTaskCreate(
                    taskTwo,          // Task function. 
                    "TaskTwo",        // String with name of task. 
                    10000,            // Stack size in bytes. 
                    NULL,             // Parameter passed as input of the task 
                    1,                // Priority of the task. 
                    NULL);            // Task handle. 
 */
  Serial2.write("{\"command\": \"request_date\"}");
  Serial2.write('\0');
  /*char buffer[255];
  size_t len = Serial2.readBytesUntil('\0', buffer, 255);
  DynamicJsonDocument json(1024);
  deserializeJson(json, buffer, len);
  execute_commands(json);*/
}

void loop()
{
  std::string txt = poll_uart2();
  if (!txt.empty())
  {
    DynamicJsonDocument command(1024*2);
    //StaticJsonDocument<1024*2> command;
    deserializeJson(command, txt.c_str(), txt.size()+1);
    execute_commands(command);
    Serial.printf("Received data of length %i: %s\n", txt.size(), txt.c_str());
  }

  //delay(2000);
}