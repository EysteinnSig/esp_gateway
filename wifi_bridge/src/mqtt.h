#pragma once

//https://github.com/256dpi/arduino-mqtt

#include <WiFi.h>
#include <MQTT.h>
#include "settings.h"
#include "measurement.h"

MQTTClient mqtt_client(512);

bool mqtt_command_update_clock = false;


bool is_MQTT_enabled()
{
    if (!settings.containsKey("mqtt")) 
    {
        //Serial.println("No config for MQTT service.");
        return false;
    }
    return true;
}

bool connect_MQTT()
{
    if (!is_MQTT_enabled())
        return false;
    if (mqtt_client.connected()) 
        return true;
    mqtt_client.setCleanSession(false);
    std::string user = settings["mqtt"]["user"].as<std::string>();
    std::string password = settings["mqtt"]["password"].as<std::string>();
    int tries = settings["mqtt"]["connect_tries"].as<int>();
    Serial.print("Connecting to MQTT ...");
    while (!mqtt_client.connect("esp32", user.c_str(), password.c_str())) {
        Serial.print(".");
        delay(500);
        tries--;
        if (tries < 0)
        {
            Serial.println(" Fail");
            return false;
        }
    }
    Serial.println(" OK");

    std::string topic_remote = "nodes/" + settings["mqtt"]["client_id"].as<std::string>() + "/remote/#";
    int qos = settings["mqtt"]["qos"].as<int>();
    Serial.printf("Subscribing to: %s\n", topic_remote.c_str());
    int max_tries = 2;
    //if (!mqtt_client.subscribe(topic_remote.c_str(), qos))
    while (!mqtt_client.subscribe(topic_remote.c_str(), qos))
    {
        Serial.println("Error subscribing"); //topic.c_str(), qos);
        lwmqtt_err_t err = mqtt_client.lastError();
        Serial.println((int)err);
        max_tries--;
        if(max_tries<=0)
            break;
        delay(100);
        
    }

    return true;
}

void MQTT_onreceived(std::string &topic, std::string &payload)
{   
    std::string topic_remote = "nodes/" + settings["mqtt"]["client_id"].as<std::string>() + "/remote";
    int topic_remote_len = topic_remote.length();
    //Serial.printf(">%s<\n", topic.substr(0, topic_remote_len).c_str());
    if (topic.substr(0, topic_remote_len) != topic_remote)
        return;
    std::string suffix = topic.substr(topic_remote_len);
    
    Serial.printf("Topic: %s\n", topic.c_str());
    Serial.printf("Suffix: %s\n", suffix.c_str());
    Serial.printf("Payload: %s\n", payload.c_str());

    if (suffix == "/restart")
        ESP.restart();
    else if (suffix == "/rtc_update")
    {
        mqtt_command_update_clock = true;
    }
    else if (suffix == "/config/update")
    {
    }
    else
        Serial.printf("Command not found: %s", suffix.c_str());
    //Serial.printf("incoming: " + topic + " - " + payload);

}
void MQTT_onreceived_adv(MQTTClient *client, char topic_c[], char bytes[], int length)
{
    std::string topic(topic_c);
    std::string payload(bytes, length);
    MQTT_onreceived(topic, payload);
}
bool init_MQTT()
{
    static WiFiClient network_client;

    if (!is_MQTT_enabled()) return false;
    std::string mqtt_server = settings["mqtt"]["host"].as<std::string>();
    uint16_t mqtt_port = settings["mqtt"]["port"].as<uint16_t>();
    IPAddress mqtt_ip;
    if (!mqtt_ip.fromString(mqtt_server.c_str()))
    {
        Serial.printf("Getting IP from DNS.\n");
        if (WiFiGenericClass::hostByName(mqtt_server.c_str(), mqtt_ip) != 1)
            return false;
    }
    mqtt_client.begin(mqtt_ip, mqtt_port, network_client);
    mqtt_client.setCleanSession(false);
    mqtt_client.onMessageAdvanced(MQTT_onreceived_adv);

    connect_MQTT();

    //std::string topic = settings["mqtt"]["subscribe_topic"].as<std::string>();
    
    //topic = "node/"+settings["mqtt"]["client_id"].as<std::string>() + "/settigns/#"
    //mqtt_client.subscribe()
    return true;
}


bool MQTT_publish(const std::string &topic, const std::string &payload)
{
    if (!is_MQTT_enabled()) return false;
    if (!mqtt_client.connected()) return false;
    //std::string topic = "nodes/"+settings["mqtt"]["client_id"].as<std::string>()+"/measurement";
    int qos = settings["mqtt"]["qos"].as<int>();
    //std::string msg = m.to_json();
    //bool ret = mqtt_client.publish(topic.c_str(), msg.c_str(), false, qos);
    //if (!ret)
    int max_tries=3;
    bool ret = true;
    while (!mqtt_client.publish(topic.c_str(), payload.c_str(), false, qos))
    {
        lwmqtt_err_t err = mqtt_client.lastError();
        Serial.printf("Failed to publish with error code: %i\n", (int)err);
        max_tries--;
        if (max_tries<=0)
        {   
            ret = false;
            break;        
        }
        delay(100);
    }
    return ret;
}

bool MQTT_publish(Measurement &m)
{   
    if (!is_MQTT_enabled()) return false;
    if (!mqtt_client.connected()) return false;
    std::string topic = "nodes/"+settings["mqtt"]["client_id"].as<std::string>()+"/measurement";
    int qos = settings["mqtt"]["qos"].as<int>();
    std::string msg = m.to_json();
    //bool ret = mqtt_client.publish(topic.c_str(), msg.c_str(), false, qos);
    //if (!ret)
    int max_tries=3;
    bool ret = true;
    while (!mqtt_client.publish(topic.c_str(), msg.c_str(), false, qos))
    {
        lwmqtt_err_t err = mqtt_client.lastError();
        Serial.printf("Failed to publish with error code: %i\n", (int)err);
        max_tries--;
        if (max_tries<=0)
        {   
            ret = false;
            break;        
        }
        delay(100);
    }
    return ret;
}




void loop_MQTT()
{
    if (is_MQTT_enabled() && mqtt_client.connected())
    {
        mqtt_client.loop();
        delay(10);  // <- fixes some issues with WiFi stability
        
    }
}