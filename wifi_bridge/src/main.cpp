
#include <esp_now.h>
#include <WiFi.h>
#include <esp_wifi.h>
#include "driver/uart.h"
#include <string>

#include "measurement.h"
#include "settings.h"
#include "mqtt.h"
#include "rtc.h"
//#define TRANSMIT 1
//#include <Adafruit_Sensor.h>

//PubSubClient client(espClient);

//RTC_DS3231 rtc;



//uint8_t peer_mac[] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
//uint8_t *peer_mac;


void print_mac(const uint8_t *mac)
{
  for (int i=0; i<6; i++) Serial.printf("%X ", mac[i]);
}


void uart_send(void * parameter)
{
  const int TXD_PIN = GPIO_NUM_17;
  const int RXD_PIN = GPIO_NUM_16;
  /*const int SERIAL_SIZE_RX = 1024;
  const uart_config_t uart_config = {
        .baud_rate = 9600,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE
    };
    uart_param_config(UART_NUM_1, &uart_config);
    uart_set_pin(UART_NUM_1, TXD_PIN, RXD_PIN, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
    // We won't use a buffer for sending data.
    uart_driver_install(UART_NUM_1, SERIAL_SIZE_RX, 0, 0, NULL, 0);*/
  Serial1.begin(9600, SERIAL_8N1, RXD_PIN, TXD_PIN);
  while (true)
  {
    //Serial1.readString()
    if (!measurements.empty())
    {

    }
  }    
}

void init_wifi()
{
  WiFi.mode(WIFI_STA);
  std::string address = settings["wifi"]["address"].as<std::string>();
  IPAddress local_IP;
  if (!local_IP.fromString(address.c_str()))
    local_IP = INADDR_NONE;
  WiFi.config(local_IP, INADDR_NONE, INADDR_NONE, INADDR_NONE);
  String hostname = "ESP32-Meteo"; // Weather station";

  WiFi.setHostname(hostname.c_str()); //define hostname
  std::string ssid = settings["wifi"]["ssid"].as<std::string>();
  std::string password = settings["wifi"]["password"].as<std::string>();
  WiFi.begin(ssid.c_str(), password.c_str());
  Serial.print("Connecting to WiFi ..");
  int maxtries = 5000;
  while (WiFi.status() != WL_CONNECTED && maxtries > 0) {
    Serial.print('.');
    delay(1000);
    maxtries--;

  }
  if (WiFi.isConnected())
    Serial.println(" OK");
  else
    Serial.println(" FAIL");
}

void setup()
{
  Serial.begin(9600);
  while (!Serial);

  const int TXD_PIN = GPIO_NUM_17;
  const int RXD_PIN = GPIO_NUM_16;
  Serial2.begin(9600, SERIAL_8N1, RXD_PIN, TXD_PIN);
  while (!Serial2);

  if(!SPIFFS.begin()){
    Serial.println("An Error has occurred while mounting SPIFFS");
    while (true);
  }
  load_settings();

  init_wifi();
  Serial.println(WiFi.macAddress());

  set_clock_from_ntp();
  //WiFi.mode(WIFI_MODE_STA);
  RtcDateTime dt;
  dt.InitWithEpoch32Time(get_clock());

  Serial.printf("System clock: %s\n", datetime2str(dt).c_str());


  
  init_MQTT();

  delay(1000);

  File file = SPIFFS.open("/espnow_peers.json","r");
  std::string jsonstr = file.readString().c_str();
  Serial.printf("Sending file content: %s\n", jsonstr.c_str());
  //Serial.printf("Sending file to sensor_bridge: espnow_peers.json");
  jsonstr = "{\"command\": \"overwritefile\",\"filename\": \"/espnow_peers.json\",\"content\":" + jsonstr + "}";

  Serial2.write(jsonstr.c_str(), jsonstr.length()+1);

  jsonstr = "{\"command\": \"set_date\", \"unixtime\": ";
  jsonstr = jsonstr + std::to_string(get_clock())+"}";
  Serial2.write(jsonstr.c_str(), jsonstr.length()+1);

  /*xTaskCreate(
                    taskTwo,          // Task function. 
                    "TaskTwo",        // String with name of task. 
                    10000,            // Stack size in bytes. 
                    NULL,             // Parameter passed as input of the task 
                    1,                // Priority of the task. 
                    NULL);            // Task handle. 
 */
}



void loop()
{
  uint8_t buff[1024];
  if (Serial2.available() > 0) 
  {
    size_t len = Serial2.readBytesUntil('\0', buff, 1024);
    Serial.printf("Received uart data: %i bytes\n", len);
    std::string payload((const char*)&buff[0], len);
    Serial.printf("%s\n", payload.c_str());

    MQTT_publish("node/esp_meteo_1/measurement", payload);

    DynamicJsonDocument json(1024*2);
    deserializeJson(json, buff);
    if (json.containsKey("command"))
    {
      std::string command_str = json["command"].as<std::string>();
      Serial.printf("Received command: %s\n", command_str.c_str());
      if (command_str == "request_date")
      {
        std::string jsonstr;
        jsonstr = "{\"command\": \"set_date\", \"unixtime\": ";
        jsonstr = jsonstr + std::to_string(get_clock())+"}";
        Serial2.write(jsonstr.c_str(), jsonstr.length()+1);
        Serial2.write('\0');
        Serial.printf("Sent command: %s\n", jsonstr.c_str());
      }
    }

    /*if (len>0)
    {
      uint32_t *i = (uint32_t*)&buff[0];
      Serial.printf("%i\n", *i);
    }*/
  }
  loop_MQTT();
  //delay(2000);
}