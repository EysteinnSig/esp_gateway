#pragma once
#include <string>
#include <array>
#include <map>

#include <ArduinoJson.h>
#include <vector>


struct Measurement
{
    uint32_t unixtime=0;
    std::map<std::string, float> variables;
    std::uint16_t idx;
    /*std::string to_url(const std::string &prefix = "")
    {
        std::string ret = prefix;
        for (auto iter=variables.begin(); iter!=variables.end(); iter++)
        {
            ret = ret +  iter->first + "=" + std::to_string(iter->second) + "&";
        }

        ret = ret + "time="+std::to_string(unixtime);
        return ret;
    } */   

    static Measurement from_json(DynamicJsonDocument &doc)
    {
        Measurement m;
        
        //auto root = doc.as<JsonObject>();

        // using C++11 syntax (preferred):
        /*for (JsonPair kv : root) {
            Serial.println(kv.key().c_str());
            Serial.println(kv.value().as<char*>());
        }*/
        JsonObject documentRoot = doc.as<JsonObject>();
        for (JsonPair keyValue : documentRoot) {
            std::string key = std::string(keyValue.key().c_str());
            if (key == "unixtime")
                m.unixtime = keyValue.value().as<uint32_t>();
            else if (key == "idx")
                m.idx = keyValue.value().as<uint16_t>();    
            else if (keyValue.value().is<float>())
               m.variables[std::string(keyValue.key().c_str())] = keyValue.value().as<float>();
        }
        return m;
    }

    std::string to_json() const
    {
        DynamicJsonDocument doc(1024);

        for (auto iter = variables.begin(); iter != variables.end(); iter++)
        {
            std::string key = iter->first;
            float value = iter->second;
            doc[key] = value;
        }
        doc["unixtime"] = unixtime;
        doc["idx"] = this->idx;
        std::string jsonstr;
        serializeJson(doc, jsonstr);
        return jsonstr;
    }
    
    float& operator[](const std::string &var)
    {
        /*auto iter = variables.find(var);
        if (iter != variables.end())
            return iter->second;
        return -3e30;*/
        return variables[var];
    }

    const float& operator[](const std::string &var) const
    {
        Measurement *m = const_cast<Measurement*>(this);
        return (*m)[var];
        /*auto iter = variables.find(var);
        if (iter != variables.end())
            return iter->second;*/
    }

    void clear()
    {
        variables.clear();
    }

    Measurement()
    {
        static std::uint16_t internal_idx = 0; // Zero is not legal idx
        internal_idx++;
        this->idx = internal_idx;
    }

    //std::array<std::pair<std::string, float>, SIZE > variables;
    /*std::array<std::string, SIZE> variables;
    std::array<float, SIZE> values;
    std::string variables[measurement_size];
    float values[measurement_size];*/

    /*int add_variable(const std::string &name)
    {
        for (int k=0; k<measurement_size; k++)
        {
            if (variables[k].empty())
            {
                variables[k] = 
            }
        }
    }*/

};


bool operator== (const Measurement& c1, const Measurement& c2)
{
    return c1.idx == c2.idx;
}


const int max_measurements = 1000;
std::vector<Measurement> measurements;

SemaphoreHandle_t measurement_mutex = xSemaphoreCreateMutex();


void add_measurement(const Measurement &m)
{
    if (!xSemaphoreTake( measurement_mutex, portMAX_DELAY))
        return;

    measurements.push_back(m);
    if (measurements.size() > max_measurements)
        measurements.erase(measurements.begin());
    xSemaphoreGive(measurement_mutex);
    
}
void del_first_measurement()
{

    if (!xSemaphoreTake( measurement_mutex, portMAX_DELAY))
        return;
    if (!measurements.empty())
        measurements.erase(measurements.begin());
    xSemaphoreGive(measurement_mutex);
}

void del_idx_measurement(uint16_t idx)
{
    if (!xSemaphoreTake( measurement_mutex, portMAX_DELAY))
        return;
    measurements.erase(
    std::remove_if(measurements.begin(), measurements.end(),
        [idx](const Measurement & m) { return m.idx == idx; }),
    measurements.end());

    xSemaphoreGive(measurement_mutex);

}
