#pragma once

#include <ArduinoJson.h>
#include "SPIFFS.h"

StaticJsonDocument<1024> settings;
void save_settings()
{
    
    settings.garbageCollect();

    File file = SPIFFS.open("/settings.json","w");
    if(!file)
    {
        Serial.println("Failed to open file for reading");
        return;
    }
    String jsonstr;
    serializeJson(settings, jsonstr);
    file.println(jsonstr);
    file.close();

    settings.garbageCollect();
}
void load_settings()
{
    File file = SPIFFS.open("/settings.json","r");
    if(!file)
    {
        Serial.println("Failed to open file for reading");
        return;
    }
    String jsonstr = file.readString();
    file.close();
    settings.clear();
    deserializeJson(settings, jsonstr);
}

void print_settings()
{
    String jsonstr;
    serializeJson(settings, jsonstr);
    Serial.println("Settings:");
    Serial.println(jsonstr);
}