#pragma once

#include <esp_now.h>
#include <WiFi.h>
#include <map>
#include <vector>
#include "measurement.h"

std::map<uint8_t[6], std::vector<uint8_t> > packages;
uint8_t peer_mac[] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

// callback when data is sent
void espnow_ondatasent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  Serial.print("Last Packet Send Status:\t");
  
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
}

void espnow_ondatarecv(const uint8_t * mac, const uint8_t *incomingData, int len) {
    Serial.print("Message incoming: ");
    Measurement sample;
    
    std::string str((char*)incomingData, len);
    Serial.println(str.c_str());
}



void espnow_init()
{
    Serial.print("Initalizing ESP-NOW: ");
    if (esp_now_init() != ESP_OK) {
        //Serial.println("Error initializing ESP-NOW");
        Serial.println("FAIL");
        return;
    }
    Serial.println("OK");

    

    esp_now_register_send_cb(espnow_ondatasent);
    esp_now_register_recv_cb(espnow_ondatarecv);
    //auto mac = WiFi.macAddress();

    // Register peer
    esp_now_peer_info_t peerInfo={};
    //uint8_t mac_address[] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
    uint8_t mac_address[] = {0x34, 0x86, 0x5D, 0x3A, 0x34, 0x0C};
    //{0x32, 0xAE, 0xA4, 0x07, 0x0D, 0x66};
    //uint8_t mac_address[6]; esp_base_mac_addr_get(mac_address);

    memcpy(peerInfo.peer_addr, mac_address, 6);
    //memcpy(peerInfo.peer_addr, peer_mac, 6);
    peerInfo.channel = 0;  
    peerInfo.encrypt = false;
    peerInfo.ifidx=WIFI_IF_STA; //WIFI_IF_AP;
    //WIFI_STA
    
    // Add peer        
    if (esp_now_add_peer(&peerInfo) != ESP_OK){
        Serial.println("Failed to add peer");
        return;
    }
}

void espnow_send(Measurement &sample, uint8_t *mac_addr = nullptr)
{
    uint8_t default_mac[] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
    if (mac_addr == nullptr)
        mac_addr = default_mac; // {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
    static uint32_t idx = 0;

    std::string json = "message: "+idx; //sample.to_json();
    const uint8_t *msg = (const uint8_t*)"Message";
    //esp_err_t error = esp_now_send(default_mac, (const uint8_t*)json.c_str(), json.length());
    esp_err_t error = esp_now_send(default_mac, msg, 7);
    if (error != ESP_OK)
    {
        Serial.printf("Error value: %i\n", error);
        Serial.printf("Error str: %s\n", esp_err_to_name(error));
    }

    idx++;
}